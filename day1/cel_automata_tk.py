import Tkinter as tk
import ttk
import game_of_life as life

class Model(object):
    def __init__(self):
        self.grid = life.test_grid
        self._grid_next = [[False] * len(l) for l in self.grid]
        self.nrows = len(self.grid)
        self.ncols = len(self.grid[0])

    def step(self):
        life.step(self.grid, self._grid_next)
        self.grid, self._grid_next = self._grid_next, self.grid


class View(object):
    R = 24
    delay = 100
    palette = ["#%02x%02x%02x" % t for t in [(251, 243, 238), (178, 38, 50)]]

    def __init__(self, model):
        self.model = model
        self.root = tk.Tk()
        self.root.title("game of life")

        self.frame = ttk.Frame(self.root, padding='3 3 12 12')
        self.frame.grid(column=0, row=0, sticky=(tk.N, tk.W, tk.E))

        self.btn_next = ttk.Button(self.frame, text='next', command=self._on_btn_next)
        self.btn_next.grid(column=0, row=0, sticky=(tk.W, tk.E))
        self.btn_p = ttk.Button(self.frame, text='play', command=self._on_btn_play)
        self.btn_p.grid(column=1, row=0, sticky=(tk.W, tk.E))

        self.grid = []
        self.canvas = tk.Canvas(self.root, width=self.R * self.model.ncols + 4,
                                height=self.R * self.model.nrows + 4, cursor='hand2')
        self.canvas.grid(column=0, row=1, columnspan=3, sticky=(tk.N, tk.W, tk.S, tk.E))

        self._create_grid()
        self._update_grid()
        self._animating = False

    def _create_grid(self):
        for i, line in enumerate(self.model.grid):
            row = []
            for j, c in enumerate(line):
                bounds = [self.R * j,
                          self.R * i,
                          self.R * j + self.R - 2,
                          self.R * i + self.R - 2]
                # translate the grid, a way to pad
                bounds = [4 + b for b in bounds]
                r = self.canvas.create_rectangle(bounds, outline='grey')
                self.canvas.tag_bind(r, '<1>', self._create_on_click(r, i, j))
                row.append(r)
            self.grid.append(row)

    def _update_grid(self):
        for i, row in enumerate(self.grid):
            for j, r in enumerate(row):
                fill_color = self.palette[ self.model.grid[i][j] ]
                self.canvas.itemconfig(self.grid[i][j], fill=fill_color)

    def _update(self):
        self.model.step()
        self._update_grid()

    def _animation(self):
        if self._animating:
            self._update()
            self.root.after(self.delay, self._animation)

    def _on_btn_next(self):
        if not self._animating:
            self._update()

    def _on_btn_play(self):
        self._animating = not self._animating
        self.canvas['cursor'] = 'arrow' if self._animating else 'hand2'
        self.btn_next['state'] = 'disabled' if self._animating else 'normal'
        self.btn_p['text'] = 'pause' if self._animating else 'play'
        if self._animating:
            self._animation()

    def _create_on_click(self, r, i, j):
        def handler(ev):
            if self._animating:
                return
            self.model.grid[i][j] = not self.model.grid[i][j]
            self._update_grid()
        return handler

if __name__ == '__main__':
    model = Model()
    view = View(model)
    view.frame.mainloop()
